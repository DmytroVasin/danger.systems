- title: "Enforce changelogs"
  id: "changelogs"
  repos: "danger/danger-js"
  code: |
    // Add a CHANGELOG entry for app changes
    const hasChangelog = includes(danger.git.modified_files, "changelog.md")
    const isTrivial = contains((danger.github.pr.body + danger.github.pr.title), "#trivial")

    if (!hasChangelog && !isTrivial) {
      warn("Please add a changelog entry for your changes.")
    }

- title: "Keep Lockfile up to date"
  id: "prose"
  repos: "danger/danger-js"
  code: |
    const packageChanged = includes(danger.git.modified_files, 'package.json');
    const lockfileChanged = includes(danger.git.modified_files, 'yarn.lock');
    if (packageChanged && !lockfileChanged) {
      const message = 'Changes were made to package.json, but not to yarn.lock';
      const idea = 'Perhaps you need to run `yarn install`?';
      warn(`${message} - <i>${idea}</i>`);
    }

- title: "Encourage smaller PRs"
  id: "merge"
  repos: "ReactiveX/rxjs"
  code: |
    var bigPRThreshold = 600;
    if (danger.github.pr.additions + danger.github.pr.deletions > bigPRThreshold) {
      warn(':exclamation: Big PR (' + ++errorCount + ')');
      markdown('> (' + errorCount + ') : Pull Request size seems relatively large. If Pull Request contains multiple changes, split each into separate PR will helps faster, easier review.');
    }

- title: "Ensure PRs have assignees"
  id: "focus"
  repos: "artsy/emission"
  code: |
    // Always ensure we assign someone, so that our Slackbot can do its work correctly
    if (pr.assignee === null) {
      fail("Please assign someone to merge this PR, and optionally include people who should review.")
    }

- title: "Encourage more testing"
  id: "testing"
  repos: "apollographql/apollo-client"
  code: |
    const hasAppChanges = modifiedAppFiles.length > 0;

    const testChanges = modifiedAppFiles.filter(filepath =>
      filepath.includes('test'),
    );
    const hasTestChanges = testChanges.length > 0;

    // Warn if there are library changes, but not tests
    if (hasAppChanges && !hasTestChanges) {
      warn(
        "There are library changes, but not tests. That's OK as long as you're refactoring existing code",
      );
    }

- title: "Highlight documentation updates"
  id: "docs"
  repos: "facebook/react-native"
  code: |
    // Check that every file touched has a corresponding test file
    const correspondingTestsForAppFiles = touchedAppOnlyFiles.map(f => {
      const newPath = path.dirname(f)
      const name = path.basename(f).replace(".ts", "-tests.ts")
      return `${newPath}/__tests__/${name}`
    })

    // New app files should get new test files
    // Allow warning instead of failing if you say "Skip New Tests" inside the body, make it explicit.
    const testFilesThatDontExist = correspondingTestsForAppFiles.filter(f => fs.existsSync(f))
    if (testFilesThatDontExist.length > 0) {
      const callout = acceptedNoTests ? warn : fail
      const output = `Missing Test Files:
        ${testFilesThatDontExist.map(f => `  - [] \`${f}\``).join("\n")}
        If these files are supposed to not exist, please update your PR body to include "Skip New Tests".
      `
      callout(output)
    }

- title: "Show useful info on a PR"
  id: "show-info"
  repos: "ReactiveX/rxjs"
  code: |
    var globalFile = 'Rx.js';
    var minFile = 'Rx.min.js';

    function sizeDiffBadge(name, value) {
      var color = 'lightgrey';
      if (value > 0) {
        color = 'red';
      } else if (value < 0) {
        color = 'green';
      }
      return 'https://img.shields.io/badge/' + name + '-' + getFormattedKB(getKB(value)) + 'KB-' + color + '.svg?style=flat-square';
    }

    //post size of build
    schedule(new Promise(function (res) {
      getSize('./dist/cjs', function (e, result) {
        var localGlobalFile = path.resolve('./dist/global', globalFile);
        var localMinFile = path.resolve('./dist/global', minFile);

        //get sizes of PR build
        var global = fs.statSync(localGlobalFile);
        var global_gzip = gzipSize.sync(fs.readFileSync(localGlobalFile, 'utf8'));
        var min = fs.statSync(localMinFile);
        var min_gzip = gzipSize.sync(fs.readFileSync(localMinFile, 'utf8'));

        // [...]

        var sizeMessage = '<img src="https://img.shields.io/badge/Size%20Diff%20%28' + releaseVersion + '%29--lightgrey.svg?style=flat-square"/>  ';
        sizeMessage += '<img src="' + sizeDiffBadge('Global', global.size - bundleGlobal.size) + '"/> ';
        sizeMessage += '<img src="' + sizeDiffBadge('Global(gzip)', global_gzip - bundle_global_gzip) + '"/> ';
        sizeMessage += '<img src="' + sizeDiffBadge('Min', min.size - bundleMin.size) + '"/> ';
        sizeMessage += '<img src="' + sizeDiffBadge('Min (gzip)', min_gzip - bundle_min_gzip) + '"/> ';
        message(sizeMessage);

        markdown('> CJS: **' + getKB(result) +
          '**KB, global: **' + getKB(global.size) +
          '**KB (gzipped: **' + getKB(global_gzip) +
          '**KB), min: **' + getKB(min.size) +
          '**KB (gzipped: **' + getKB(min_gzip) + '**KB)');

        res();
      });
    }));


- title: "Ensure Project Consistency"
  id: "flow"
  repos: "apache/incubator-weex"
  code: |
    const jsFiles = danger.git.created_files.filter(path => path.endsWith("js"));

    function absolute (relPath) {
      return path.resolve(__dirname, relPath)
    }

    const flowIgnorePaths = [
      'node_modules','test', 'build', 'examples', 'doc', 'android', 'ios', 'bin', 'dist', 'flow-typed'
    ].map(function (rel) {
      return absolute(rel)
    });

    // new js files should have `@flow` at the top
    const unFlowedFiles = jsFiles.filter(filepath => {
      let i = 0
      const len = flowIgnorePaths.length
      while (i < len) {
        const p = flowIgnorePaths[i]
        if (absolute(filepath).indexOf(p) > -1) {
          // ignore this file because it's in the flow-ignore-paths.
          return false;
        }
        i++
      }
      const content = fs.readFileSync(filepath);
      return !content.includes("@flow");
    });

    if (unFlowedFiles.length > 0) {
      warn(
        `These new JS files do not have Flow enabled: ${unFlowedFiles.join(", ")}`
      );
    }

- title: "Highlight SemVer breakages"
  id: "semver"
  repos: "styled-components/styled-components"
  code: |
    // Changes to these files may need SemVer bumps
    const semverBumpFiles = ['ThemeProvider.js', 'StyledComponent.js', 'StyledNativeComponent.js']
    semverBumpFiles.forEach(file => {
      if (jsModifiedFiles.includes(file)) {
        warn('Changes to #{file} might be SemVer major changes.')
      }
    })
